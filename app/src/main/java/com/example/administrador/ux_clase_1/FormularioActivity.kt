package com.example.administrador.ux_clase_1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.administrador.ux_clase_1.models.PersonaModel
import kotlinx.android.synthetic.main.activity_formulario.*

class FormularioActivity : AppCompatActivity() {
    var datos = ArrayList<PersonaModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formulario)
    }

    override fun onResume() {
        super.onResume()

        btnGuardar.setOnClickListener{
            val nombre = etName.text.toString()
            val genero = spGenero.selectedItem.toString()
            val edad = etEdad.text.toString()

            if (nombre == ""){
                tilName.error = "El campo es requerido"
                return@setOnClickListener
            }

            if(edad == ""){
                etEdad.error = "El campo es requerido"
                return@setOnClickListener
            } else
                etEdad.error = null

            datos.add(PersonaModel(nombre,genero,edad.toInt()))
            Toast.makeText(this, "Se registro!",Toast.LENGTH_SHORT).show()

        }

        btnListar.setOnClickListener{
            val intent = Intent()

        }

    }
}
