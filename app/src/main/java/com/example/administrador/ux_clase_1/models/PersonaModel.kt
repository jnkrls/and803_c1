package com.example.administrador.ux_clase_1.models

class PersonaModel (
    var nombre: String,
    var genero: String,
    var edad: Int
)